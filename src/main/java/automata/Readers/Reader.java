package automata.Readers;

public abstract class Reader {
    private String mLastString;

    protected Reader(){
        mLastString = "";
    }

    public abstract boolean open();
    protected abstract String readStringInternal();
    public abstract void close();

    public String readString(){
        String str = readStringInternal();
        mLastString = str;

        return str;
    }

    public long readLong() throws NumberFormatException {
        String str = readString();

        return Long.parseLong(str, 10);
    }

    protected abstract boolean isEndOfRead();

    public boolean isEndOfTrace(){
        return isEndOfRead() || mLastString.equals("STOP");
    }


}
