package automata.Readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CSVTraceReader extends Reader {
    private BufferedReader mReader;

    private String[] mCurStringBuffer;
    private int mCurPosAtBuffer;

    private String mPath;
    private boolean mIsEnd;

    final static private String CSV_DELIM = ";";

    public CSVTraceReader(final String path) {
        super();

        mReader = null;

        mCurStringBuffer = null;
        mCurPosAtBuffer = 0;

        mPath = path;
        mIsEnd = false;
    }

    @Override
    public boolean open() {
        try{
            mReader = Files.newBufferedReader(Paths.get(mPath), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("IO exception at [OPEN]");
            return false;
        }

        String startString = readStringInternal();
        if (!startString.equals("START")){
            System.out.println("Trace does not start from key \"START\"!");
        }

        return true;
    }

    @Override
    protected String readStringInternal() {
        if (null != mCurStringBuffer && mCurPosAtBuffer < mCurStringBuffer.length){
            return mCurStringBuffer[mCurPosAtBuffer++];
        }

        try{
            String str = mReader.readLine();
            if (null != str){
                mCurStringBuffer =  str.split(CSV_DELIM);
                mCurPosAtBuffer = 0;

                return mCurStringBuffer[mCurPosAtBuffer++];
            }

            mIsEnd = true;
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("IO exception at [readStringInternal]");
        }

        return "";
    }

    @Override
    public void close() {
        try {
            mReader.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("IO exception at [close]");
        }
    }

    @Override
    protected boolean isEndOfRead() {
        return mIsEnd;
    }
}
