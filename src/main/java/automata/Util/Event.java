package automata.Util;

public class Event{
    public enum Type{
        BEGIN,
        END
    }

    private String mName;
    private int mVertexFrom;
    private int mVertexTo;
    private long mGlobalTimeStart;
    private long mDuration;
    private Type mType;

    Event(final String name, int vertextFrom, int vertextTo, long startTime, long duration, boolean isBegin){
        mName = name;
        mVertexFrom = vertextFrom;
        mVertexTo = vertextTo;
        mGlobalTimeStart = startTime;
        mDuration = duration;
        mType = isBegin ? Type.BEGIN : Type.END;
    }

    public String getName() {
        return mName;
    }

    public int getVertexFrom() {
        return mVertexFrom;
    }

    public int getVertexTo() {
        return mVertexTo;
    }

    public long getGlobalTimeStart() {
        return mGlobalTimeStart;
    }

    public long getDuration() {
        return mDuration;
    }

    public Type getType() {
        return mType;
    }
}
