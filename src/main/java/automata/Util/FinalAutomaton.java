package automata.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FinalAutomaton {
    private int mColors;
    private int mStates;
    private int mTransitions;
    private int mSameEventTransitions;
    private int mClocks;

    public int colors[];
    public int t[][][][];
    public boolean reset[][][][];
    public int cmin[][][][][];
    public int cmax[][][][][];
    public boolean useClock[][][][][];

    public FinalAutomaton(int vertexCount, int n, int e, int k, int clocks){
        mColors = vertexCount;
        mStates = n;
        mTransitions = e;
        mSameEventTransitions = k;
        mClocks = clocks;

        // clocks + 1 == T
        colors = new int[vertexCount];
        t = new int[n][e][2][k];
        reset = new boolean[n][e][k][clocks + 1];
        cmin = new int[n][e][2][k][clocks + 1];
        cmax = new int[n][e][2][k][clocks + 1];
        useClock = new boolean[n][e][2][k][clocks + 1];
    }

    private HashMap<String, String[]> getOutput(BufferedReader reader) {
        HashMap<String, String[]> res = new HashMap<>();
        try {
            String cur;
            while ((cur = reader.readLine()) != null && cur.length() > 0) {
                String[] name = cur.split(": ");
                if (name.length != 2) {
                    // parse error
                    System.err.println(String.format("Error while parsing output %s", cur));
                } else {
                    String[] params = name[1].substring(1, name[1].length() - 1).split(", ");
                    res.put(name[0], params);
                }
            }
        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
        }

        return res;
    }

    public boolean parseResults(BufferedReader reader) {
        HashMap<String, String[]> map = getOutput(reader);
        if (map.isEmpty()) {
            return false;
        }

        String[] tColors = map.get("colors");
        String[] tNums = map.get("t");
        String[] resetBools = map.get("resets");
        String[] cminNums = map.get("cmin");
        String[] cmaxNums = map.get("cmax");
        String[] cUsed = map.get("clock_used");

        int indexColors = 0, indexT = 0, indexReset = 0, indexCmin = 0, indexCmax = 0;
        for (int i = 0; i < mColors; ++i) {
            colors[i] = Integer.valueOf(tColors[indexColors++]) - 1;
        }

        for (int i = 0; i < mStates; ++i) {
            for (int j = 0; j < mTransitions; ++j) {
                for (int c = 0; c < 2; ++c) {
                    for (int l = 0; l < mSameEventTransitions; ++l) {
                        t[i][j][c][l] = Integer.valueOf(tNums[indexT++]);
                        for (int m = 0; m <= mClocks; ++m) {
                            cmin[i][j][c][l][m] = Integer.valueOf(cminNums[indexCmin++]);
                            cmax[i][j][c][l][m] = Integer.valueOf(cmaxNums[indexCmax++]);
                            useClock[i][j][c][l][m] = Boolean.parseBoolean(cUsed[indexCmax - 1]);
                        }
                    }
                }

                for (int c = 0; c < mSameEventTransitions; ++c) {
                    for (int l = 0; l <= mClocks; ++l) {
                        reset[i][j][c][l] = resetBools[indexReset++].equalsIgnoreCase("true");
                        // ONLY FOR DEBUG
//                            if (reset[i][j][c][l] && l != mClocks) {
//                                reset[i][j][c][l] = false;
//                            }
                    }
                }
            }
        }


        return true;
    }

    public boolean checkTrace(InputTree tree, Trace trace) {
        int cur = colors[0];
        long[] curClocks = new long[mClocks + 1];
        for (int i = 0; i < mClocks + 1; ++i) {
            curClocks[i] = -1;
        }

        ArrayList<Trace.TraceElem> elems = trace.getElems();
        loop: for (int i = 0; i < elems.size(); ++i) {
            Trace.TraceElem elem = elems.get(i);
            int to = tree.getEdgeKind(elem.name);
            int isBegin = elem.isBegin ? 0 : 1;
            long time = elem.time;

            int foundIndex = -1;
            for (int j = 0; j < mSameEventTransitions; ++j) {
                if (t[cur][to][isBegin][j] > 0 && t[cur][to][isBegin][j] < mStates + 1) {
                    if (isBegin == 0) {
                        for (int k = 0; k <= mClocks; ++k) {
                            if (reset[cur][to][j][k]) {
                                curClocks[k] = time;
                            }
                        }
                    }

                    boolean guardSatisfied = true;
                    for (int k = 0; k <= mClocks; ++k) {
                        if (cmax[cur][to][isBegin][j][k] > 0 && useClock[cur][to][isBegin][j][k]) {
                            if (curClocks[k] == -1 || !((time - curClocks[k]) <= cmax[cur][to][isBegin][j][k] &&
                                                        (time - curClocks[k]) >= cmin[cur][to][isBegin][j][k])) {
                                guardSatisfied = false;
                                break;
                            }
                        }
                    }

                    if (guardSatisfied) {
                        cur = t[cur][to][isBegin][j] - 1;
                        continue loop;
                    }
                }
            }

            if (i < elems.size() - 1) {
                return false;
            }
        }

        return true;
    }
}
