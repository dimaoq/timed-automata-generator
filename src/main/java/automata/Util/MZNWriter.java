package automata.Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class MZNWriter {
    private String mFileName, mIncluded;
    private StringBuilder mText;

    private void addInclude(){
        mText.append("include \"").append(mIncluded).append("\";\n\n");
    }

    public MZNWriter(final String fileName, final String includedMZN){
        mFileName = fileName;
        mIncluded = includedMZN;

        mText = new StringBuilder();
        addInclude();
    }

    public void write(final String text){
        mText.append(text);
    }

    public boolean push(){
        PrintWriter pw;
        try {
            pw = new PrintWriter(new File(mFileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        pw.print(mText.toString());
        pw.close();

        mText.delete(0, mText.length());
        addInclude();

        return true;
    }
}
