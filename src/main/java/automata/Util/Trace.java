package automata.Util;

import java.util.ArrayList;

public class Trace {

    static public class TraceElem {
        public String name;
        public boolean isBegin;
        public long time;

        public TraceElem(String pName, boolean pIsBegin, long pTime) {
            name = pName;
            isBegin = pIsBegin;
            time = pTime;
        }
    }

    private ArrayList<TraceElem> mElems;

    Trace(){
        mElems = new ArrayList<>();
    }

    public ArrayList<TraceElem> getElems() {
        return mElems;
    }

    public void addTrace(TraceElem elem) {
        mElems.add(elem);
    }
}
