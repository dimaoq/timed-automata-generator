package automata.Util;

import automata.Readers.CSVTraceReader;

import java.util.*;

public class InputTree {
    private HashMap<String, Integer> eventIndices;
    private Stack<Event> eventBegins;
    public ArrayList<Event> events;
    public ArrayList<Trace> traces;
    private int stateCount;
    private int edgeCount;

    private InputTree() {
        eventIndices = new HashMap<>();
        eventBegins = new Stack<>();
        events = new ArrayList<>();
        traces = new ArrayList<>();
        stateCount = 1;
        edgeCount = 0;
    }

    public int getStateCount() {
        return stateCount;
    }

    public int getEdgeCount() {
        return edgeCount;
    }

    public int getEdgeKind(final String name) {
        return eventIndices.get(name);
    }

    public int getEventKindsCount(){
        return eventIndices.size();
    }

    public String getEventName(int index) {
        for (Map.Entry<String, Integer> entry : eventIndices.entrySet()){
            if (entry.getValue() == index) {
                return entry.getKey();
            }
        }

        throw new NoSuchElementException();
    }

    private static boolean parseTrace(InputTree tree, final String fileName){
        CSVTraceReader reader = new CSVTraceReader(fileName);

        if (!reader.open()){
            System.out.println(String.format("File is not accessed: %s", fileName));
            return false;
        }

        tree.eventBegins.clear();
        boolean first = true, isValid = true;
        long startTime = 0, prevTime = 0;
        int curVertex = 1;
        Trace trace = new Trace();
        while (isValid && !reader.isEndOfTrace()) {
            String garbage = reader.readString();
            if (garbage.equals("STOP")){
                break;
            }

            String name = reader.readString();
            String type = reader.readString().toUpperCase();
            boolean isBegin = type.equals("B");
            long time = reader.readLong() - startTime;
            if (first){
                startTime = time;
                time = 0;
                first = false;
            }
            reader.readString();

            trace.addTrace(new Trace.TraceElem(name, isBegin, time));

            if (time < prevTime) {
                isValid = false;
                System.out.println(String.format("Transition time is negative at time %d!", prevTime));
            } else {
                Integer index = tree.eventIndices.get(name);
                if (null == index) {
                    if (!isBegin) {
                        isValid = false;
                        System.out.println(String.format("End event %s occurs before begin event!", name));
                    } else {
                        int newIndex = curVertex + 1;
                        if (curVertex == 1) {
                            newIndex = tree.stateCount + 1;
                        }

                        Event newEvent =
                                new Event(name, curVertex, newIndex, time, 0, true);
                        tree.events.add(newEvent);
                        tree.eventIndices.put(name, tree.eventIndices.size());
                        tree.edgeCount++;
                        tree.eventBegins.push(newEvent);
                        tree.stateCount++;

                        curVertex = newIndex;
                    }
                } else {
                    if (isBegin) {
                        int newIndex = curVertex + 1;
                        if (curVertex == 1) {
                            newIndex = tree.stateCount + 1;
                        }
                        Event newEvent =
                                new Event(name, curVertex, newIndex, time, 0, true);
                        tree.events.add(newEvent);
                        tree.edgeCount++;
                        tree.eventBegins.push(newEvent);
                        tree.stateCount++;
                        curVertex = newIndex;
                    } else {
                        Event begin = tree.eventBegins.pop();
                        if (!begin.getName().equals(name) || time < begin.getGlobalTimeStart()) {
                            isValid = false;
                            System.out.println(String.format("End event %s occurs before begin event!", name));
                        } else {
                            Event newEvent =
                                    new Event(name, curVertex, ++curVertex, time, time - begin.getGlobalTimeStart(), false);
                            tree.events.add(newEvent);
                            tree.stateCount++;
                            tree.edgeCount++;
                        }
                    }
                }
            }

            prevTime = time;
        }

        tree.traces.add(trace);
        reader.close();

        return isValid && tree.eventBegins.empty();
    }

    public static InputTree readTraces(final List<String> traces) {
        InputTree ret = new InputTree();

        for (String trace : traces) {
            if (!parseTrace(ret, trace)) {
                System.out.println(String.format("Trace %s skipped due to parse errors", trace));
            }
        }

        return ret;
    }
}
