package automata;

import automata.Util.Event;
import automata.Util.FinalAutomaton;
import automata.Util.InputTree;
import automata.Util.MZNWriter;
import guru.nidi.graphviz.attribute.Label;
import guru.nidi.graphviz.engine.*;
import guru.nidi.graphviz.model.*;

import static guru.nidi.graphviz.model.Factory.*;
import static java.lang.Math.min;

import guru.nidi.graphviz.service.SystemUtils;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TimedAutomata {
    private static void writeConsole(String string){
        System.out.println(string);
    }

    private final String NEW_MZN_FILE = "solver2.mzn"; // special test case
    private final String INCLUDED_FILE = "model_automata.mzn";

    private final int PNG_WIDTH = 800;

    private List<String> mTraces;
    private int mClocks;
    private long mTdiff;

    private interface ITreeWriter{
        String write(int i);
    }

    private class ModelInfo{
        int n, e, k, c;

        ModelInfo(int ns, int es, int ks, int cs) {
            n = ns;
            e = es;
            k = ks;
            c = cs;
        }
    }

    private void writeMZNHeader(MZNWriter writer, final ModelInfo info, int vertexCount, int inputEventsSize, long tmax, long tdiff) {
        writer.write(String.format(
                        "V = %d;\n" +
                        "E = %d;\n" +
                        "M = %d;\n" +
                        "K = %d;\n" +
                        "N = %d;\n" +
                        "C = %d;\n" +
                        "TMAX = %d;\n" +
                        "MAX_CDIFF = %d;\n\n",
                        vertexCount, inputEventsSize, info.e, info.k, info.n, info.c, tmax, tdiff));
    }

    private void writeMZNContent(MZNWriter writer, int inputEventsSize, String what, ITreeWriter func) {
        writer.write(String.format("%s = [", what));
        for (int i = 0; i < inputEventsSize; ++i){
            writer.write(func.write(i));
            if (i < inputEventsSize - 1){
                writer.write(", ");
            }
        }
        writer.write("];\n");
    }

    private String formSolveScript(String solver, String mznfile) {
//        String filename = FilenameUtils.getBaseName(mznfile);
//        StringBuilder sb = new StringBuilder();
//        sb.append("mzn2fzn ").append(mznfile).append(" && ");
//        sb.append(solver).append(" -use_cp_sat ").append(filename).append(".fzn | ");
//        sb.append("solns2out ").append(filename).append(".ozn");

        return solver + " " + mznfile;
    }

    public TimedAutomata(List<String> traces, int c, long tdiff) {
        mTraces = traces;
        mClocks = c;
        mTdiff = tdiff;
    }

    private void saveGraph(FinalAutomaton automaton, InputTree tree, ModelInfo info) throws IOException{
        MutableGraph graph = mutGraph("Automaton");
        graph.setDirected(true);
        MutableNode[] nodes = new MutableNode[info.n];
        for (int i = 0; i < info.n; ++i) {
            nodes[i] = mutNode(i == (automaton.colors[0]) ? "start" : String.valueOf(i));
            graph.add(nodes[i]);
        }

        for (int i = 0; i < info.n; ++i) {
            for (int j = 0; j < info.e; ++j) {
                for (int q = 0; q < 2; ++q) {
                    for (int w = 0; w < info.k; ++w) {
                        if (automaton.t[i][j][q][w] != info.n + 1) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(tree.getEventName(j)).append(q == 0 ? " :B" : " :E").append("\n");
                            for (int c = 0; c <= info.c; ++c) {
                                String cName = c == info.c ? "t" : "c" + String.valueOf(c + 1);
                                if (q == 0 && automaton.reset[i][j][w][c]) {
                                    sb.append("r(").append(cName).append(")\n");
                                }
                                if (automaton.cmax[i][j][q][w][c] > 0) {
                                    sb.append(String.valueOf(automaton.cmin[i][j][q][w][c])).
                                            append(" <= ").append(cName).append(" <= ").
                                            append(String.valueOf(automaton.cmax[i][j][q][w][c])).append("\n");
                                }
                            }
                            nodes[i].addLink(to(nodes[automaton.t[i][j][q][w] - 1]).with(Label.of(sb.toString())));
                        }
                    }
                }
            }
        }

        GraphvizCmdLineEngine engine = new GraphvizCmdLineEngine();
        engine.setDotOutputFile(new File("").getAbsolutePath(), "source");
        Graphviz.useEngine(engine);
        Graphviz.fromGraph(graph).render(Format.PNG).toFile(new File("./test.png"));
    }

    private void printTree(InputTree tree) throws IOException {
        int n = tree.getStateCount();
        MutableGraph graph = mutGraph("Input tree");
        graph.setDirected(true);
        MutableNode[] nodes = new MutableNode[n];
        for (int i = 0; i < n; ++i) {
            nodes[i] = mutNode(Integer.toString(i));
            graph.add(nodes[i]);
        }

        for (Event e : tree.events) {
            String text = e.getName();
            if (e.getType() == Event.Type.BEGIN) {
                text += ": B";
            } else {
                text += ": E";
            }
            text += "\n";
            text += "t=" + Long.toString(e.getGlobalTimeStart());
            nodes[e.getVertexFrom() - 1].addLink(to(nodes[e.getVertexTo() - 1]).with(Label.of(text)));
        }

        GraphvizCmdLineEngine engine = new GraphvizCmdLineEngine();
        engine.setDotOutputFile(new File("").getAbsolutePath(), "source2");
        Graphviz.useEngine(engine);
        Graphviz.fromGraph(graph).rasterize(Rasterizer.SALAMANDER).toFile(new File("./test2.png"));
    }

    public void solve(String solver) {
        InputTree tree = InputTree.readTraces(mTraces);
        if (tree.getEdgeCount() == 0) {
            writeConsole("No data was being parsed, exit");
            return;
        }

        try {
            printTree(tree);
            return;
        } catch (Exception e){

        }

        int inputEventsSize = tree.getEdgeCount();
        int vertexCount = tree.getStateCount();
        int eventKindsCount = tree.getEventKindsCount();

        writeConsole(String.format("Ready to MZN, %d states, %d transitions", vertexCount, inputEventsSize));

        long tmax = -1;
        for (int i = 0; i < inputEventsSize; ++i) {
            tmax = Math.max(tmax, tree.events.get(i).getGlobalTimeStart());
        }

        int clocks = mClocks;
        long maxDistance = (mTdiff < 0) ? (tmax / 2) : mTdiff;

        MZNWriter writer = new MZNWriter(NEW_MZN_FILE, INCLUDED_FILE);

        String shellCmd = "";
        String shellParam = "";
        if (SystemUtils.IS_OS_WINDOWS) {
            shellCmd = "cmd";
            shellParam = "/c";
        } else {
            if (SystemUtils.IS_OS_LINUX) {
                shellCmd = "/bin/bash";
                shellParam = "-c";
            }
        }

        String runScript = formSolveScript(solver, NEW_MZN_FILE);
        File curDir = new File("").getAbsoluteFile();
        for (int n = 1; ; ++n) {
            for (int k = 1; k <= n; ++k) {
                FinalAutomaton automaton = new FinalAutomaton(vertexCount, n, eventKindsCount, k, clocks);
                ModelInfo info = new ModelInfo(n, eventKindsCount, k, clocks);

                writeMZNHeader(writer, info, vertexCount, inputEventsSize, tmax, maxDistance);

                writeMZNContent(writer, inputEventsSize, "vertex_from", i -> String.valueOf(tree.events.get(i).getVertexFrom()));
                writeMZNContent(writer, inputEventsSize, "vertex_to", i -> String.valueOf(tree.events.get(i).getVertexTo()));
                writeMZNContent(writer, inputEventsSize, "t_edge", i -> String.valueOf(tree.events.get(i).getGlobalTimeStart()));
                writeMZNContent(writer, inputEventsSize, "edge_type", i -> tree.events.get(i).getType() == Event.Type.BEGIN ? "1" : "2");
                writeMZNContent(writer, inputEventsSize, "edge_kind", i -> String.valueOf(tree.getEdgeKind(tree.events.get(i).getName()) + 1));

                if (!writer.push()){
                    System.err.println(String.format("Push has failed: file: %s\n include: %s\nN = %d, K = %d\n",
                            NEW_MZN_FILE, INCLUDED_FILE, n, k));
                    continue;
                }

                ProcessBuilder pb = new ProcessBuilder(Arrays.asList(shellCmd, shellParam, runScript));
                pb.directory(curDir);
                pb.redirectErrorStream(true);
                try {
                    Process p = pb.start();

                    BufferedReader bufReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    boolean res = automaton.parseResults(bufReader);
                    bufReader.close();

                    p.waitFor();
                    p.destroy();

                    if (res) {
                        writeConsole(String.format("Timed automaton with %d states and %d transitions found!", n, k));
                        writeConsole("Checking traces for validness");
                        boolean ok = true;
                        for (int i = 0; ok && (i < tree.traces.size()); ++i) {
                            if (!automaton.checkTrace(tree, tree.traces.get(i))) {
                                ok = false;
                                writeConsole(String.format("Trace %d didn't pass", i));
                                return;
                            }
                        }

                        if (ok) {
                            writeConsole("Traces are valid, saving results");
                            saveGraph(automaton, tree, info);
                            return;
                        }
                        writeConsole("Continuing with new model params");
                    } else {
                        writeConsole(String.format("Finish timed automaton with %d states and %d transitions cannot be constructed", n, k));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class CmdArgs{
        @Option(required = true, name = "-c", usage = "number of clocks in model")
        int clocks;

        @Option(name = "-tdiff", usage = "maximum difference between min and max time for each transition, default value = TMAX / 2")
        long tdiff = -1;

        @Option(required = true, name = "-solver", usage = "solver for model")
        String solver;

        @Argument(required = true)
        List<String> traces = new ArrayList<>();

        static boolean validate(CmdArgs args) {
            return !((args.traces.size() == 0) || (args.clocks <= 0));
        }
    }

    public static void main(String[] args) {
        CmdArgs cmdArgs = new CmdArgs();
        CmdLineParser argsParser = new CmdLineParser(cmdArgs);
        try{
            argsParser.parseArgument(args);
            if (!CmdArgs.validate(cmdArgs)) {
                writeConsole("Wrong arguments passed");
            } else {
                TimedAutomata automata = new TimedAutomata(cmdArgs.traces, cmdArgs.clocks, cmdArgs.tdiff);
                automata.solve(cmdArgs.solver);
            }
        } catch (CmdLineException e) {
            System.err.println(e.getLocalizedMessage());
            argsParser.printUsage(System.err);
        }
    }
}
