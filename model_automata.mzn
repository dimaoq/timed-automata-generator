% Author: Dmitry Vinokurov

% given variables
int: V; % vertices
int: E; % input events
int: M; % number of various types of events
int: K; % maximum number of transitions
int: N; % colors count
int: C; % clocks count
int: TMAX; % maximal time
int: MAX_CDIFF; % maximal distance between CMAX and CMIN

set of int: s_vertices = 1..V;
set of int: s_allEdges = 1..E;
set of int: s_difEdges = 1..M;
set of int: s_transitions = 1..K;
set of int: s_colors = 1..N+1;
set of int: s_clocks = 1..(C + 1); % (C + 1)'th clock = global clock t
set of int: s_time = 0..TMAX; % time counter
set of int: s_edge_type = 1..2; % type of edge: begin = 1, end = 2

% given arrays
array[1..E] of 1..V: vertex_from; % for every edge: start vertex
array[1..E] of 1..V: vertex_to; % for every edge: end vertex
array[1..E] of 0..TMAX: t_edge; % timestamps for every edge
array[1..E] of 1..M: edge_kind; % kind of edge
array[1..E] of 1..2: edge_type; % type of every edge

% output variables
array[1..V] of var 1..N: colors; % colors of vertices
array[1..N, 1..M, 1..2, 1..K] of var s_colors: t; % output transitions
array[1..N, 1..M, 1..2, 1..K, 1..E] of var bool: fired; % fired transitions
array[1..N, 1..M, 1..2, 1..K, 1..E] of var bool: fired_only;
array[1..C+1, 0..TMAX] of var s_time: clocks; % clock values at each timestamp
array[1..N, 1..M, 1..K, 1..C+1] of var bool: reset; % true if clock resets
array[1..C+1, 0..TMAX] of var bool: reset_st; % helper reset array
array[1..N, 1..M, 1..2, 1..K, 1..C+1] of var s_time: cmin; % minimal time value for each clock for each timestamp
array[1..N, 1..M, 1..2, 1..K, 1..C+1] of var s_time: cmax; % maximal time value for each clock for each timestamp
array[1..N, 1..M, 1..2, 1..K, 1..C+1] of var bool: clock_used; 
array[1..C+1, 0..TMAX] of var bool : have_been_reset;

% first vertex with color 1
constraint colors[1] = 1;

% assign transitions
constraint forall (e in 1..E) (
	exists (k in 1..K)
		(t[colors[vertex_from[e]], edge_kind[e], edge_type[e], k] = colors[vertex_to[e]]
        /\ colors[vertex_to[e]] != N+1
		/\ fired_only[colors[vertex_from[e]], edge_kind[e], edge_type[e], k, e])
);

constraint forall (e in 1..E where edge_type[e] == 1) (
    colors[vertex_from[e]] = 1 -> reset[1, edge_kind[e], 1, C+1]
);


% set fired_only
constraint forall(n in 1..N, e in 1..E, k in 1..K) (
	(fired_only[n, edge_kind[e], edge_type[e], k, e]) <-> (fired[n, edge_kind[e], edge_type[e], k, e] /\
                                                              (forall (k2 in 1..K where k2 != k)
                                                                   (not fired[n, edge_kind[e], edge_type[e], k2, e])))
);

constraint forall(n in 1..N, m in 1..M, typ in 1..2, k in 1..K) (
    not clock_used[n, m, typ, k, C+1]
);

% assign fired array
constraint forall(n in 1..N, m in 1..M, typ in 1..2, k in 1..K, e in 1..E) (
	fired[n, m, typ, k, e] <->
		(forall (i in 1..C+1) (
             have_been_reset[i, t_edge[e]] /\ clock_used[n, m, typ, k, i] -> (clocks[i, t_edge[e]] >= cmin[n, m, typ, k, i] /\ clocks[i, t_edge[e]] <= cmax[n, m, typ, k, i])))
             %%clock_used[n, m, typ, k, i] -> (have_been_reset[i, t_edge[e]] /\ clocks[i, t_edge[e]] >= cmin[n, m, typ, k, i] /\ clocks[i, t_edge[e]] <= cmax[n, m, typ, k, i])))
%%           clock_used[n, m, typ, k, i] -> (clocks[i, t_edge[e]] >= cmin[n, m, typ, k, i] /\ clocks[i, t_edge[e]] <= cmax[n, m, typ, k, i])))
%           have_been_reset[i, t_edge[e]] -> (clocks[i, t_edge[e]] >= cmin[n, m, typ, k, i] /\ clocks[i, t_edge[e]] <= cmax[n, m, typ, k, i])))
);

% define maximum distance between min/max clock time
constraint forall(n in 1..N, m in 1..M, typ in 1..2, k in 1..K, c in 1..C+1) (
    %cmax[n, m, typ, k, c] - cmin[n, m, typ, k, c] <= MAX_CDIFF
    %/\
    cmax[n, m, typ, k, c] - cmin[n, m, typ, k, c] >= 0
);

constraint forall(n in 1..N, m in 1..M, typ in 1..2, k in 1..K, c in 1..C) (
    cmax[n, m, typ, k, c] <= cmax[n, m, typ, k, C+1]
);

% special case - global time clock
constraint reset_st[C + 1, 0] /\ have_been_reset[C+1, 0];

constraint forall(t in 1..TMAX) (
    not reset_st[C + 1, t]
);

constraint forall (c in s_clocks) (
    exists (t in s_time) (reset_st[c, t])
);

constraint forall (c in 1..C) (
    exists (n in 1..N, m in 1..M, k in 1..K) (reset[n, m, k, c] /\ t[n, m, 1, k] < N+1)
);

constraint forall(n in 1..N, k in 1..K, e in 1..E, c in 1..C+1 where edge_type[e] == 1) (
    fired_only[n, edge_kind[e], edge_type[e], k, e] /\ reset[n, edge_kind[e], k, c] -> reset_st[c, t_edge[e]] /\ clocks[c, t_edge[e]] = 0 
);

constraint forall (c in 1..C, t in 0..TMAX) (
    reset_st[c, t] <-> exists (n in 1..N, e in 1..E, k in 1..K) (reset[n, edge_kind[e], k, c] /\ fired_only[n, edge_kind[e], 1, k, e])
);


% reset -> clock = 0
constraint forall(c in 1..C+1, t in s_time) (
  (reset_st[c, t]) -> (clocks[c, t] = 0 /\ have_been_reset[c, t])
);

constraint forall (c in 1..C+1, t in 0..TMAX-1) (
    have_been_reset[c, t] -> have_been_reset[c, t+1]
);

constraint forall (c in 1..C, t in 0..TMAX-1) (
    not have_been_reset[c, t] /\ not reset_st[c, t] -> not have_been_reset[c, t+1]
);

%constraint forall (c in 1..C) (
%    not have_been_reset[c, 0]
%);

% !reset -> clock++
constraint forall(c in s_clocks, t in s_time where t > 0) (
  (not reset_st[c, t]) -> (clocks[c, t] = clocks[c, t - 1] + 1)
);

constraint forall(n in 1..N, m in 1..M) (
    not (t[n, m, 1, 1] < N+1 /\ t[n, m, 2, 1] < N+1)
);

%constraint sum(n in 1..N, m in 1..M, typ in 1..2, k in 1..K) (bool2int(t[n, m, typ, k] != N+1)) = MAX_CDIFF;



solve minimize sum(n in 1..N, m in 1..M, typ in 1..2, k in 1..K) (bool2int(t[n, m, typ, k] != N+1));
%%solve minimize sum(n in 1..N, e in 1..M, typ in 1..2, k in 1..K, c in 1..C+1) (cmax[n, e, typ, k, c] - cmin[n, e, typ, k, c]);
%%solve satisfy;
%);


output
 % ["reset1: ", show(reset_st[1, 0]), "\n"] ++
 % ["reset2: ", show(reset_st[2, 0]), "\n"] ++
 % ["reset3: ", show(reset_st[3, 0]), "\n"] ++
 % ["reset4: ", show(reset_st[4, 0]), "\n"] ++
 % ["reset5: ", show(reset_st[5, 0]), "\n"] ++
 % ["reset6: ", show(reset_st[6, 0]), "\n"] ++
 % ["reset7: ", show(reset_st[7, 0]), "\n"] ++
  ["colors: ", show(colors), "\n"] ++
%  ["clocks: ", show(clocks), "\n"] ++
  ["resets: ", show(reset), "\n"] ++
%  ["reset_st: ", show(reset_st), "\n"] ++
%  ["have_been_reset: ", show(have_been_reset), "\n"] ++
  ["t: ", show(t), "\n"] ++
  ["cmin: ", show(cmin), "\n"] ++
  ["cmax: ", show(cmax), "\n"]
  %["clock_used: ", show(clock_used), "\n"]
